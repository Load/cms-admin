<?php

namespace Backend;

use Phalcon\Mvc\Router;

class Module extends \Ss\Mvc\Module {
	static function routing(Router $router) {
		$backend = new \Phalcon\Mvc\Router\Group(array(
			'module' => 'backend',
		));

		$backend->setPrefix('/admin');

		$backend->add('', array(
			'controller' => 'index',
			'action'     => 'index',
		))->setName('admin/home');

		$router->mount($backend);

		return $router;
	}
}