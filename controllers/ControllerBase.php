<?php

namespace Backend\Controllers;

use Ss\Mvc\Controller;

class ControllerBase extends Controller {
	public function initialize() {
		$this->assets->collection('cssHead')->addCss('backend/assets/css/init.css');
		$this->assets->collection('cssHead')->addCss('backend/assets/css/init2.css');
	}
}

